# Spring Boot Kafka Demo

### Things To do list

### Install Kafka

1. Install Kafka from [Official Kafka Website](https://kafka.apache.org/documentation/#topicconfigs)
2. Navigate to the folder. Extract it.
3. Start ZooKeeper service: `$ bin/zookeeper-server-start.sh config/zookeeper.properties`
4. Start Kafka service: `$ bin/kafka-server-start.sh config/server.properties`
5. Create Kafka Topics: `bin/kafka-topics.sh --create --topic quickstart-events --bootstrap-server localhost:9092`
6. List All Kafka Topics: `bin/kafka-topics.sh --list --zookeeper localhost:2181`
7. Create Kafka Producer: `bin/kafka-console-producer.sh --topic quickstart-events --bootstrap-server localhost:9092`
8. Read Kafka
   Topics: `bin/kafka-console-consumer.sh --topic quickstart-events --from-beginning --bootstrap-server localhost:9092`
9. Open your terminal:

```shell
curl --location 'localhost:8080/api/v1/kafka/publish' \
--header 'Content-Type: application/json' \
--data '{
    "id" : 2,
    "firstName": "Uzumaki",
    "lastName": "Naruto"
}'
```

```shell
curl --location 'localhost:8080/api/v1/kafka/publish?message=naruto'
```

### Docker Compose

Creating a Kafka Topic

```shell
$ docker-compose exec kafka kafka-topics.sh --create --topic hendi
  --partitions 1 --replication-factor 1 --bootstrap-server kafka:9092
```

Publishing and Consuming Messages

```shell
$ docker-compose exec kafka kafka-console-consumer.sh --topic hendi
  --from-beginning --bootstrap-server kafka:9092
```

```shell
$ docker-compose exec kafka kafka-console-producer.sh --topic hendi
  --broker-list kafka:9092
```
