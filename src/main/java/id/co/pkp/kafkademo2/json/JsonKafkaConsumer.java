package id.co.pkp.kafkademo2.json;

import id.co.pkp.kafkademo2.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : kafka-demo2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/23
 * Time: 14:53
 * To change this template use File | Settings | File Templates.
 */
@Service
@Slf4j
public class JsonKafkaConsumer {
    @KafkaListener(topics = "${spring.kafka.topic-json.name}", groupId = "${spring.kafka.consumer.group-id}")
    public void consume(User user) {
        log.info(String.format("Json message received -> %s", user.toString()));
    }
}
